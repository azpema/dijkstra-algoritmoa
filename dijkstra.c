#include <stdio.h>
#include <stdlib.h>
#include "dijkstra.h"
#define INFINITY 99999
#define MIN(a,b) (((a)<(b))?(a):(b))

int main(int argc, char *argv[]){
  int n, i, j, src, dst, cost, flag, dijkstra_src, iternode, mincost;
  int  *visited, *graph, *dijkstra;



  printf("Idatzi nodo kopurua:\n");
  if(scanf("%d", &n) == EOF){
    printf("Karaktere okerra. Irtetzen...\n");
    return 1;
  } 

  printf("Idatzi nodoen arteko loturak eta haien kostuak (iturburua helburua kostua). Amaitu duzunean, '-1' idatzi\n");
  
  visited = (int*) malloc (n * sizeof(int));
  graph = (int *) malloc (n * n * sizeof(int));

  for(i=0; i<n; i++){
    visited[i]=0;
    for(j=0; j<n; j++){
        if(i == j) graph[i*n +j] = 0;
        else       graph[i*n +j] = INFINITY;
    }
  }

  flag = 1;
  while(flag){
    if(scanf("%d %d %d", &src, &dst, &cost) == EOF){
        printf("Karaktere okerra. Irtetzen...\n");
        return 1;
    }
    if(src == -1){
      flag = 0;
    }else{
      graph[src*n + dst] = cost;
      graph[dst*n + src] = cost;
    }
  }
  
  printGraph(graph, n);

  printf("Nondik hasita nahi dituzu jakin distantzia laburrenak?\n");
  
  if(scanf("%d", &dijkstra_src)==EOF){
    printf("Karaktere okerra. Irtetzen...\n");
    return 1;
  }


  dijkstra = (int*) malloc(n*sizeof(int));
  //Dijkstra hasieraketa
  for(i=0; i<n; i++){
    if(i==dijkstra_src) dijkstra[i]=0;
    else dijkstra[i]=INFINITY;
  }

  //Dijkstra algoritmoa
  mincost=INFINITY;
  for(i=0; i<n; i++){   //Nodo guztiak pasa behar dira visited[n] listara
    for(j=0; j<n; j++){   //Aukeratu zein nodo sartuko den visited[n] listan
      if(visited[j]==0){
        if(dijkstra[j] <= mincost){//Aukeratu distantzia txikienera dagoen eta oraindik 
            iternode = j;                         //bisitatua izan ez den nodoa
            mincost = dijkstra[j];
        } 
      }
    }
    visited[iternode] = 1;
    mincost=INFINITY;
    for(j=0; j<n; j++){ //Egiaztatu bizilagunak
      if(graph[iternode*n + j] < INFINITY){
          dijkstra[j] = MIN(dijkstra[j], dijkstra[iternode] + graph[iternode*n + j]);
      }
    }
    printf("\n\n%d. iterazioa, %d. nodoa sartu da listan\n", i, iternode);
    shortestPaths(dijkstra, n);
  }

  free(visited);
  free(graph);
  free(dijkstra);
  return 0;
}

void printGraph(int* graph, int n){
  int i, j;
  for(i=0; i<n; i++){
    for(j=0; j<n; j++){
      if(graph[i*n +j]==INFINITY) printf("%3s ", "-");
      else                        printf("%3d ", graph[i*n +j]);
    }
    printf("\n");
  }
}

void shortestPaths(int* dijkstra, int n){
  int i;
  for(i=0; i<n; i++){
    if(dijkstra[i]==INFINITY) printf("D(%d) = %3s\n", i, "-");
    else                      printf("D(%d) = %3d\n", i, dijkstra[i]);
    
  }
}